import telebot
import settings as cfg

bot = telebot.TeleBot(cfg.access_token)


@bot.message_handler(content_types=['text'])
def get_text_messages(message):
    if message.text == "/start":
        bot.send_message(message.from_user.id, "Hi!\n"
                                               "I can start deployment pipeline of WP project if you want.\n"
                                               "Send /deploy to start it!")
    elif message.text == "/deploy":
        bot.send_message(message.from_user.id, "DEPLOYING...")
    else:
        bot.send_message(message.from_user.id, "Cannot understand you :c\n"
                                               "Try send /deploy to start deployment pipeline.")


bot.polling(none_stop=True, interval=0)
